namespace CacheAppPOC.Api
{
    public interface ICacheKey
    {
        string Value { get; }
    }
}