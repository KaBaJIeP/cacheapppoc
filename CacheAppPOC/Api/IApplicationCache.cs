namespace CacheAppPOC.Api
{
    public interface IApplicationCache : IInMemoryCache, IDistributedCache
    {
        ICacheKeyBuilder GetKeyBuilder(string key);
    }
}