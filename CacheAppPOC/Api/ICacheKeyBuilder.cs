namespace CacheAppPOC.Api
{
    public interface ICacheKeyBuilder
    {
        ICacheKeyBuilder WithSuffix(string suffix);
        ICacheKeyBuilder WithPrefix(string prefix);
        ICacheKeyBuilder WithSiteName(string siteName);
        ICacheKey Build();
    }
}