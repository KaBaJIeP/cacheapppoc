using System;

namespace CacheAppPOC.Api
{
    public interface ICache
    {
        bool Set<T>(ICacheKey key, T value, TimeSpan validFor);
        bool Set<T>(ICacheKey key, T value);

        T Get<T>(ICacheKey key);
    
        void Remove(ICacheKey key);
    
        void Clear();
    }
}