namespace CacheAppPOC.Api
{
    public interface IInMemoryCache
    {
        ICache InMemory { get; }
    }
}