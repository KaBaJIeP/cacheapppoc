namespace CacheAppPOC.Api
{
    public interface IDistributedCache
    {
        ICache Distributed { get; }
    }
}