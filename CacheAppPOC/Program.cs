﻿using System;
using CacheAppPOC.Api;
using CacheAppPOC.Domain;

namespace CacheAppPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            object someValueForCache = new object();
            IApplicationCache appCache = new ApplicationCache();

            ICacheKey key = appCache.GetKeyBuilder("MY_KEY")
                .WithPrefix("MY_PREFIX")
                .WithSuffix("MY_SUFFIX")
                .Build();
            
            appCache.InMemory.Set(key, someValueForCache);
            
            ICacheKey key2 = appCache.GetKeyBuilder("MY_KEY_2")
                .WithSiteName("MY_SITE_NAME")
                .Build();
            
            appCache.Distributed.Set(key2, someValueForCache);
            
            Console.WriteLine("Hello World!");
        }
    }
}