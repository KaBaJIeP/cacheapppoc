using CacheAppPOC.Api;

namespace CacheAppPOC.Domain
{
    public class CacheKeyBuilder : ICacheKeyBuilder
    {
        private string _key;
  
        // We can add some separator ? or '.' (dot) by default
        private CacheKeyBuilder(string key)
        {
            _key = key;
        }
        
        public static ICacheKeyBuilder Create(string key)
        {
            return new CacheKeyBuilder(key);
        }
        
        public ICacheKeyBuilder WithSuffix(string suffix)
        {
            _key = $"{_key}.{suffix}";
            return this;
        }

        public ICacheKeyBuilder WithPrefix(string prefix)
        {
            _key = $"{prefix}.{_key}";
            return this;
        }

        // We can encapsulate 'siteName' in ICacheKeyBuilder by DI and this method can be 
        // ICacheKeyBuilder WithSiteName() { _key = $"{ICallerContext.SiteName}.{_key}"; return this; }
        
        public ICacheKeyBuilder WithSiteName(string siteName)
        {
            _key = $"{siteName}.{_key}";
            return this;
        }

        public ICacheKey Build()
        {
            return CacheKey.Create(_key);
        }
    }
}