using CacheAppPOC.Api;

namespace CacheAppPOC.Domain
{
    public class ApplicationCache : IApplicationCache
    {
        public ICache InMemory { get; }
        
        public ICache Distributed { get; }
        
        public ICacheKeyBuilder GetKeyBuilder(string key)
        {
            return CacheKeyBuilder.Create(key);
        }
    }
}