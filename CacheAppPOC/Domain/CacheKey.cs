using CacheAppPOC.Api;

namespace CacheAppPOC.Domain
{
    public class CacheKey : ICacheKey
    {
        public string Value { get; }
        
        private CacheKey(string value)
        {
            Value = value;
        }
   
        public static ICacheKey Create(string value)
        {
            return new CacheKey(value);
        }
    }
}